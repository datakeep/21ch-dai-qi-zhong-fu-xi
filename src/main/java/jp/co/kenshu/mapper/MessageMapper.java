package jp.co.kenshu.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.MessageEntity;

public interface MessageMapper {

	//投稿一件取得
    MessageEntity getMessage(int id);

    //投稿全件取得
    List<MessageEntity> getMessageAll();

    //投稿をデータベースに追加
    //引数複数の時Param使う必要ある
    int insertMessage(@Param("message")String message, @Param("createdDate")Date createdDate, @Param("updatedDate")Date updatedDate);

    //投稿削除
    //serviceで送られてきたmessageIdとisStopped=1をmessagesテーブルに挿入
    int deleteMessage(@Param("id")int id, @Param("isStopped")int isStopped);
}